import 'dart:math';

// import 'package:buttonbomb_flutter/animation.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_vibrate/flutter_vibrate.dart';
// import "package:flutter/services.dart";
import 'package:animated_text_kit/animated_text_kit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //var size = MediaQuery.of(context).size;
    final ButtonStyle style = ElevatedButton.styleFrom(
      primary: Colors.amber[700],
      textStyle: const TextStyle(fontSize: 20),
    );

    return MaterialApp(
      title: 'Flutter Demo',
      // theme: ThemeData(
      //   primarySwatch: Colors.blue,
      // ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("ButtonBomb"),
        ),
        body: Main(style: style),
      ),
    );
  }
}

class Main extends StatefulWidget {
  const Main({
    Key? key,
    required this.style,
  }) : super(key: key);
  final ButtonStyle style;

  @override
  State<Main> createState() => _MainState();
}

class _MainState extends State<Main> {
  int _counter = 0;
  int _targetNumber = 0;
  final int _targetNumberMin = 10;
  final int _targetNumberMax = 50;
  @override
  void initState() {
    super.initState();
    // _targetNumber = 5;
    _targetNumber = _targetNumberMin +
        Random().nextInt(_targetNumberMax - _targetNumberMin);
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  showAlertDialog(BuildContext context) {
    // Init
    AlertDialog dialog = AlertDialog(
      title: const Text("你輸了！！"),
      actions: [
        ElevatedButton(
            child: const Text("重來"),
            onPressed: () {
              // reset target number
              _targetNumber = _targetNumberMin +
                  Random().nextInt(_targetNumberMax - _targetNumberMin);
              // _targetNumber = 3;
              setState(() {
                _counter = 0;
              });
              Navigator.pop(context);
            }),
      ],
    );

    // Show the dialog
    showDialog(
        context: context,
        builder: (BuildContext context) {
          // return dialog;
          return Center(
            child: SizedBox(
              width: 250.0,
              child: DefaultTextStyle(
                style: const TextStyle(
                  fontSize: 50.0,
                  color: Colors.white,
                  fontFamily: 'Canterbury',
                ),
                child: AnimatedTextKit(
                    animatedTexts: [
                      ScaleAnimatedText('你輸了！！'),
                    ],
                    onTap: () {
                      // print("Tap Event");
                    },
                    totalRepeatCount: 2,
                    pause: const Duration(milliseconds: 500),
                    onFinished: () {
                      Navigator.pop(context);

                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return dialog;
                        },
                      );
                    }),
              ),
            ),
          );
        });
  }

  void _resetTargetNubmer() {
    // _targetNumber = 4;
    _targetNumber = _targetNumberMin +
        Random().nextInt(_targetNumberMax - _targetNumberMin);
    setState(() {
      _counter = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    // print("$_counter");
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          child: ElevatedButton(
            onPressed: _resetTargetNubmer,
            child: const Text("Reset Bomb"),
          ),
        ),
        const SizedBox(height: 100),
        Center(
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              // const Text.rich(
              Text(
                "隨機從 0 - $_targetNumberMax 中挑選一個數字爆炸",
                style: const TextStyle(fontSize: 20),
              ),
              // ),
              const SizedBox(height: 30),
              Center(
                child: Text("$_counter", style: const TextStyle(fontSize: 100)),
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                style: widget.style,
                onPressed: () {
                  _checkBomb(context);
                },
                child: const Text("Press"),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _checkBomb(BuildContext context) async {
    if (_targetNumber - 1 == _counter) {
      // print("bingo");
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //     builder: (context) => AnimaBuilder(),
      //   ),
      // );
      showAlertDialog(context);
    } else {
      _incrementCounter();
    }
  }

  // static Future<void> vibrate() async {
  //   await SystemChannels.platform.invokeMethod<void>('HapticFeedback.vibrate');
  // }
}
